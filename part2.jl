### A Pluto.jl notebook ###
# v0.17.7

using Markdown
using InteractiveUtils

# ╔═╡ 6e00b510-b456-11ec-2644-cb19a985b621
using PlutoUI

# ╔═╡ d4223ac6-c16e-45e9-8432-8e0956ccc1e8
using  MLDataset:MNIST

# ╔═╡ 093058f8-de9f-429b-ba45-d1d0714a8fcd
using Flux, Plots, Statistics, EvalMetrics,Printf

# ╔═╡ 719dc1b2-39f9-4b08-bf7c-d41737cd2b50
using Flux.Data: DataLoader

# ╔═╡ f1cf90f2-8d88-4feb-8cf3-b8489e815655
using Flux: onehotbatch, logitcrossentropy, params, update!, train!, onecold, @epochs, onehot

# ╔═╡ 4f648f72-a656-492c-8ce0-c3a385b5bbf8
using Images, FileIO, ImageMagick, ImageIO

# ╔═╡ 17a44366-9ce4-4133-8b0f-031c30cd23c0
using PrettyTables

# ╔═╡ ad79116d-6236-4654-8622-964642a0601a
trainPath = "C:\\Users\\vilho\\Desktop\\School\\chest_xray\\train"

# ╔═╡ 5b0bf080-4f73-42d3-900d-6a4cdc0d44e9
testPath = "C:\\Users\\vilho\\Desktop\\School\\chest_xray\\test"

# ╔═╡ 4c532728-409b-4703-a4ad-527e7e2c34d6
train_dir = readdir(trainPath)

# ╔═╡ 51a935b4-e8c2-4b7a-9a4c-4d1a3727ef96
trainData, testData = []

# ╔═╡ fd9c0e2e-b0e2-43a3-8881-da376b3e34b1
trainLabels, testLabels = []

# ╔═╡ 3d1f84de-e5c9-4af4-b9cd-755ade20d9e2
for trn in train_dir
	if trn == "NORMAL"
		nom = joinpath(trainPath,trn)
		for alltrainImg in readdir(nom)
			img = load(joinpath(nom,alltrainImg))
			img = Gray.(img)
			img = imresize(img,(28,28))
			push!(trainData,img)
			push!(trainLabels,trn)
		end
		
	elseif trn == "PNEUMONIA"
		pne = joinpath(trainPath,trn)
		for alltrainImg in readdir(pne)
			img = load(joinpath(pne,alltrainImg))
			img = Gray.(img)
			img = imresize(img,(28,28))
			push!(trainData,img)
			push!(trainLabels,trn)
		end
	end
end

# ╔═╡ 79fd5749-f46c-43db-9867-31a0ee1334ed
trainData

# ╔═╡ 94855084-0162-4c33-b542-b40a64f85bc9
trainLabels

# ╔═╡ 016e4ad3-b5f1-42a3-b084-b3957c9d4151
size(trainData)

# ╔═╡ b974f8a2-6965-4f5e-a396-b6175290af08
size(trainLabels)

# ╔═╡ abdfc4f3-c840-4934-a724-e9a0b09c0cc0
for trn in test_dir
	if trn == "NORMAL"
		nom1 = joinpath(testPath,trn)
		for alltestImg in readdir(nom1)
			img = load(joinpath(nom1,alltestImg))
			img = Gray.(img)
			img = imresize(img,(28,28))

			push!(testData,img)
			push!(testLabels,trn)
		end
		
	elseif trn == "PNEUMONIA"
		pne1 = joinpath(testPath,trn)
		for alltestImg in readdir(pne1)
			img = load(joinpath(pne1,alltestImg))
			img = Gray.(img)
			img = imresize(img,(28,28))
			
			push!(testData,img)
			push!(testLabels,trn)
		end
	end
end

# ╔═╡ 18011983-1da8-47cf-b045-9bb02fad0056
testData

# ╔═╡ 72d8caa6-07ef-4645-abc2-680f5762b90d
testLabels

# ╔═╡ 5be1a79c-fae5-46ae-822f-83c88aa3ba04
size(testData)

# ╔═╡ 3e3e5a06-88fe-46b9-94f9-5f06244d30eb
size(testLabels)

# ╔═╡ 400d7709-d3d9-4161-a1f1-061a75c6079f
begin
	ytrain = onehotbatch(trainLabels,["NORMAL","PNEUMONIA"])
	ytest = onehotbatch(testLabels,["NORMAL","PNEUMONIA"])
end

# ╔═╡ 6a06fdac-134a-45d7-8b7f-ab5e5ece5f7e
testLabels[600]

# ╔═╡ 528908c6-6853-4109-bb7f-447657f51da9
onecold(ytest)[600]

# ╔═╡ e7fe8008-a01e-4483-aea8-a02f4c14f16e
preprocess(img) = Float64.(img)

# ╔═╡ c2c466ea-f7df-44d5-816e-d6a68f9f3389
reshaping(x) = reshape(x,(28,28,1))

# ╔═╡ 3898d756-79c9-4026-bcda-aaf794f8bd36
begin
	xtrain = [preprocess(p) for p in trainData]	
	xtrain = [reshaping(x_train) for x_train in xtrain] 
	xtrain = Flux.batch(xtrain)
	
	xtest = [preprocess(p) for p in testData]	
	xtest = [reshaping(x_test) for x_test in xtest]
	xtest = Flux.batch(xtest)
end

# ╔═╡ 47ce70d2-f2ea-47ac-9762-b7684af064ea
begin
	model = Chain(
            Conv((5, 5), 1=>6, relu),
            MaxPool((2, 2)),
            Conv((5, 5), 6=>16, relu),
            MaxPool((2, 2)),
            x -> reshape(x, :, size(x, 4)),
            Dense(*(4,4,16), 120, relu), 
            Dense(120, 84, relu), 
            Dense(84, 2)
          )
end

# ╔═╡ 7af8e4ee-6754-4cdc-abdd-3b6dc2dec06d
opt = ADAM()

# ╔═╡ e1d7f5c2-cdd3-441f-942f-429c6355aa92
oss(x, y) = logitcrossentropy(model(x), y)

# ╔═╡ dbf71386-4b85-438f-8c27-79879f1f1a47
ps = params(model)

# ╔═╡ be6b4c29-97e0-411c-9d3c-a3dfb4de67a5
train_loss = Float64[]

# ╔═╡ c0b00506-d0e7-4f93-b338-26aa24fbc0e4
function update_loss!()
        push!(train_loss, mean(loss(xtrain,ytrain)))
	
		@printf "Train loss = %.7f, " train_loss[end]
end

# ╔═╡ 3f39c934-bfc1-494e-940c-a5c1d3717a57
@epochs 30 train!(loss, ps,train_dataset, opt;
	cb = Flux.throttle(update_loss!, 5))

# ╔═╡ 4b50e0cb-23d4-40d1-a654-e0d856399138
plot(train_loss, xlabel="Iterations", title="Model Training", label="Train loss", lw=2, alpha=0.9)

# ╔═╡ d6879c8c-5acc-4e74-b718-18bd975eef7e
convertValue(x) = x == 2 ? 0 : x 

# ╔═╡ 98608d66-8ec3-49c2-b7b8-03dfb8637a97
begin
	testActualValues = onecold(ytest)
	testActualValues = [convertValue(xt) for xt in testActualValues]
end

# ╔═╡ 22b96045-920b-4f56-9c3e-08e24034c658
begin
	testPredictValues = onecold(model(xtest))
	testPredictValues = testPredictValues.==1
end

# ╔═╡ ab678882-fa81-4e45-b5dd-c56dce5da014
size(ytest)

# ╔═╡ 67fa02a7-9a52-4289-8f11-c5f567da4385
cm  = ConfusionMatrix(testActualValues, testPredictValues)

# ╔═╡ 865306cd-7022-41e5-adcf-97736698e3b0
confusion_matrix = [cm.tp cm.fn
					cm.fp	cm.tn]

# ╔═╡ 13647710-d95e-4857-b8d8-15593a750c0a
header = ["","NORMAL","PNEUMONIA"]

# ╔═╡ c3dcf1d7-83e2-48d2-8535-929be78ae051
data = hcat(header[2:3],confusion_matrix)

# ╔═╡ 5e580bb1-91f0-4862-b93c-31d4c9da7598
accuracy(testActualValues, testPredictValues)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
Flux = "587475ba-b771-5e3f-ad9e-33799f191a9c"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

[[AbstractFFTs]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "485ee0867925449198280d4af84bdb46a2a404d0"
uuid = "621f4979-c628-5d54-868e-fcf4e3e8185c"
version = "1.0.1"

[[AbstractTrees]]
git-tree-sha1 = "03e0550477d86222521d254b741d470ba17ea0b5"
uuid = "1520ce14-60c1-5f80-bbc7-55ef81b5835c"
version = "0.3.4"

[[Adapt]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "af92965fb30777147966f58acb05da51c5616b5f"
uuid = "79e6a3ab-5dfb-504d-930d-738a2a938a0e"
version = "3.3.3"

[[Artifacts]]
deps = ["Pkg"]
git-tree-sha1 = "c30985d8821e0cd73870b17b0ed0ce6dc44cb744"
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"
version = "1.3.0"

[[BFloat16s]]
deps = ["LinearAlgebra", "Test"]
git-tree-sha1 = "4af69e205efc343068dc8722b8dfec1ade89254a"
uuid = "ab4f0b2a-ad5b-11e8-123f-65d77653426b"
version = "0.1.0"

[[Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[CEnum]]
git-tree-sha1 = "215a9aa4a1f23fbd05b92769fdd62559488d70e9"
uuid = "fa961155-64e5-5f13-b03f-caf6b980ea82"
version = "0.4.1"

[[CUDA]]
deps = ["AbstractFFTs", "Adapt", "BFloat16s", "CEnum", "CompilerSupportLibraries_jll", "DataStructures", "ExprTools", "GPUArrays", "GPUCompiler", "LLVM", "Libdl", "LinearAlgebra", "Logging", "MacroTools", "NNlib", "Pkg", "Printf", "Random", "Reexport", "Requires", "SparseArrays", "Statistics", "TimerOutputs"]
git-tree-sha1 = "e4b37e96b0ff53f46b13b49d7e9091b154757dc4"
uuid = "052768ef-5323-5732-b1bb-66c8b64840ba"
version = "2.4.3"

[[ChainRules]]
deps = ["ChainRulesCore", "Compat", "LinearAlgebra", "Random", "Reexport", "Requires", "Statistics"]
git-tree-sha1 = "422db294d817de46668a3bf119175080ab093b23"
uuid = "082447d4-558c-5d27-93f4-14fc19e9eca2"
version = "0.7.70"

[[ChainRulesCore]]
deps = ["Compat", "LinearAlgebra", "SparseArrays"]
git-tree-sha1 = "4b28f88cecf5d9a07c85b9ce5209a361ecaff34a"
uuid = "d360d2e6-b24c-11e9-a2a3-2a2ae2dbcce4"
version = "0.9.45"

[[CodecZlib]]
deps = ["TranscodingStreams", "Zlib_jll"]
git-tree-sha1 = "ded953804d019afa9a3f98981d99b33e3db7b6da"
uuid = "944b1d66-785c-5afd-91f1-9de20f533193"
version = "0.7.0"

[[ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[Colors]]
deps = ["ColorTypes", "FixedPointNumbers", "Reexport"]
git-tree-sha1 = "417b0ed7b8b838aa6ca0a87aadf1bb9eb111ce40"
uuid = "5ae59095-9a9b-59fe-a467-6f913c188581"
version = "0.12.8"

[[CommonSubexpressions]]
deps = ["MacroTools", "Test"]
git-tree-sha1 = "7b8a93dba8af7e3b42fecabf646260105ac373f7"
uuid = "bbf7d656-a473-5ed7-a52c-81e309532950"
version = "0.3.0"

[[Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "96b0bc6c52df76506efc8a441c6cf1adcb1babc4"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.42.0"

[[CompilerSupportLibraries_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "8e695f735fca77e9708e795eda62afdb869cbb70"
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"
version = "0.3.4+0"

[[DataAPI]]
git-tree-sha1 = "cc70b17275652eb47bc9e5f81635981f13cea5c8"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.9.0"

[[DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[DiffResults]]
deps = ["StaticArrays"]
git-tree-sha1 = "c18e98cba888c6c25d1c3b048e4b3380ca956805"
uuid = "163ba53b-c6d8-5494-b064-1a9d43ac40c5"
version = "1.0.3"

[[DiffRules]]
deps = ["LogExpFunctions", "NaNMath", "Random", "SpecialFunctions"]
git-tree-sha1 = "d8f468c5cd4d94e86816603f7d18ece910b4aaf1"
uuid = "b552c78f-8df3-52c6-915a-8e097449b14b"
version = "1.5.0"

[[Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[DocStringExtensions]]
deps = ["LibGit2"]
git-tree-sha1 = "b19534d1895d702889b219c382a6e18010797f0b"
uuid = "ffbed154-4ef7-542d-bbb7-c09d3a79fcae"
version = "0.8.6"

[[ExprTools]]
git-tree-sha1 = "56559bbef6ca5ea0c0818fa5c90320398a6fbf8d"
uuid = "e2ba6199-217a-4e67-a87a-7c52f15ade04"
version = "0.1.8"

[[FillArrays]]
deps = ["LinearAlgebra", "Random", "SparseArrays"]
git-tree-sha1 = "693210145367e7685d8604aee33d9bfb85db8b31"
uuid = "1a297f60-69ca-5386-bcde-b61e274b549b"
version = "0.11.9"

[[FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[Flux]]
deps = ["AbstractTrees", "Adapt", "CUDA", "CodecZlib", "Colors", "DelimitedFiles", "Functors", "Juno", "LinearAlgebra", "MacroTools", "NNlib", "Pkg", "Printf", "Random", "Reexport", "SHA", "Statistics", "StatsBase", "Test", "ZipFile", "Zygote"]
git-tree-sha1 = "287705d01ab510afe075b0165a159b9e5a4bf082"
uuid = "587475ba-b771-5e3f-ad9e-33799f191a9c"
version = "0.12.1"

[[ForwardDiff]]
deps = ["CommonSubexpressions", "DiffResults", "DiffRules", "LinearAlgebra", "LogExpFunctions", "NaNMath", "Preferences", "Printf", "Random", "SpecialFunctions", "StaticArrays"]
git-tree-sha1 = "1bd6fc0c344fc0cbee1f42f8d2e7ec8253dda2d2"
uuid = "f6369f11-7733-5829-9624-2563aa707210"
version = "0.10.25"

[[Functors]]
git-tree-sha1 = "223fffa49ca0ff9ce4f875be001ffe173b2b7de4"
uuid = "d9f16b24-f501-4c13-a1f2-28368ffc5196"
version = "0.2.8"

[[GPUArrays]]
deps = ["AbstractFFTs", "Adapt", "LinearAlgebra", "Printf", "Random", "Serialization", "Statistics"]
git-tree-sha1 = "df5b8569904c5c10e84c640984cfff054b18c086"
uuid = "0c68f7d7-f131-5f86-a1c3-88cf8149b2d7"
version = "6.4.1"

[[GPUCompiler]]
deps = ["DataStructures", "InteractiveUtils", "LLVM", "Libdl", "Scratch", "Serialization", "TimerOutputs", "UUIDs"]
git-tree-sha1 = "c853c810b52a80f9aad79ab109207889e57f41ef"
uuid = "61eb1bfa-7361-4325-ad38-22787b887f55"
version = "0.8.3"

[[IRTools]]
deps = ["InteractiveUtils", "MacroTools", "Test"]
git-tree-sha1 = "7f43342f8d5fd30ead0ba1b49ab1a3af3b787d24"
uuid = "7869d1d1-7146-5819-86e3-90919afe41df"
version = "0.4.5"

[[InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[IrrationalConstants]]
git-tree-sha1 = "7fd44fd4ff43fc60815f8e764c0f352b83c49151"
uuid = "92d709cd-6900-40b7-9082-c6be49f344b6"
version = "0.1.1"

[[JLLWrappers]]
deps = ["Preferences"]
git-tree-sha1 = "abc9885a7ca2052a736a600f7fa66209f96506e1"
uuid = "692b3bcd-3c85-4b1f-b108-f13ce0eb3210"
version = "1.4.1"

[[Juno]]
deps = ["Base64", "Logging", "Media", "Profile"]
git-tree-sha1 = "07cb43290a840908a771552911a6274bc6c072c7"
uuid = "e5e0dc1b-0480-54bc-9374-aad01c23163d"
version = "0.8.4"

[[LLVM]]
deps = ["CEnum", "Libdl", "Printf", "Unicode"]
git-tree-sha1 = "f57ac3fd2045b50d3db081663837ac5b4096947e"
uuid = "929cbde3-209d-540e-8aea-75f648917ca0"
version = "3.9.0"

[[LibGit2]]
deps = ["Printf"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[LinearAlgebra]]
deps = ["Libdl"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[LogExpFunctions]]
deps = ["DocStringExtensions", "IrrationalConstants", "LinearAlgebra"]
git-tree-sha1 = "3d682c07e6dd250ed082f883dc88aee7996bf2cc"
uuid = "2ab3a3ac-af41-5b50-aa03-7779005ae688"
version = "0.3.0"

[[Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[Media]]
deps = ["MacroTools", "Test"]
git-tree-sha1 = "75a54abd10709c01f1b86b84ec225d26e840ed58"
uuid = "e89f7d12-3494-54d1-8411-f7d8b9ae1f27"
version = "0.5.0"

[[Missings]]
deps = ["DataAPI"]
git-tree-sha1 = "bf210ce90b6c9eed32d25dbcae1ebc565df2687f"
uuid = "e1d29d7a-bbdc-5cf2-9ac0-f12de2c33e28"
version = "1.0.2"

[[Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[NNlib]]
deps = ["Adapt", "ChainRulesCore", "Compat", "LinearAlgebra", "Pkg", "Requires", "Statistics"]
git-tree-sha1 = "80b8360670f445d88b3475e88b33bbcc92f7866e"
uuid = "872c559c-99b0-510c-b3b7-b6c96a88d5cd"
version = "0.7.19"

[[NaNMath]]
git-tree-sha1 = "b086b7ea07f8e38cf122f5016af580881ac914fe"
uuid = "77ba4419-2d1f-58cd-9bb1-8ffee604a2e3"
version = "0.3.7"

[[OpenLibm_jll]]
deps = ["Libdl", "Pkg"]
git-tree-sha1 = "d22054f66695fe580009c09e765175cbf7f13031"
uuid = "05823500-19ac-5b8b-9628-191a04bc5112"
version = "0.7.1+0"

[[OpenSpecFun_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "9db77584158d0ab52307f8c04f8e7c08ca76b5b3"
uuid = "efe28fd5-8261-553b-a9e1-b2916fc3738e"
version = "0.5.3+4"

[[OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[Pkg]]
deps = ["Dates", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "UUIDs"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[Preferences]]
deps = ["TOML"]
git-tree-sha1 = "d3538e7f8a790dc8903519090857ef8e1283eecd"
uuid = "21216c6a-2e73-6563-6e65-726566657250"
version = "1.2.5"

[[Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[Profile]]
deps = ["Printf"]
uuid = "9abbd945-dff8-562f-b5e8-e1ebf5ef1b79"

[[REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[Random]]
deps = ["Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[Requires]]
deps = ["UUIDs"]
git-tree-sha1 = "838a3a4188e2ded87a4f9f184b4b0d78a1e91cb7"
uuid = "ae029012-a4dd-5104-9daa-d747884805df"
version = "1.3.0"

[[SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[Scratch]]
deps = ["Dates"]
git-tree-sha1 = "0b4b7f1393cff97c33891da2a0bf69c6ed241fda"
uuid = "6c6a2e73-6563-6170-7368-637461726353"
version = "1.1.0"

[[Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[SortingAlgorithms]]
deps = ["DataStructures"]
git-tree-sha1 = "b3363d7460f7d098ca0912c69b082f75625d7508"
uuid = "a2af1166-a08f-5f64-846c-94a0d3cef48c"
version = "1.0.1"

[[SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[SpecialFunctions]]
deps = ["ChainRulesCore", "IrrationalConstants", "LogExpFunctions", "OpenLibm_jll", "OpenSpecFun_jll"]
git-tree-sha1 = "2735e252e72ee0367ebdb10b6148343fd15c2481"
uuid = "276daf66-3868-5448-9aa4-cd146d93841b"
version = "1.8.3"

[[StaticArrays]]
deps = ["LinearAlgebra", "Random", "Statistics"]
git-tree-sha1 = "74eaf352c0cef1e32ce7394bcc359d9199a28cf7"
uuid = "90137ffa-7385-5640-81b9-e52037218182"
version = "1.3.6"

[[Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[StatsAPI]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "c3d8ba7f3fa0625b062b82853a7d5229cb728b6b"
uuid = "82ae8749-77ed-4fe6-ae5f-f523153014b0"
version = "1.2.1"

[[StatsBase]]
deps = ["DataAPI", "DataStructures", "LinearAlgebra", "LogExpFunctions", "Missings", "Printf", "Random", "SortingAlgorithms", "SparseArrays", "Statistics", "StatsAPI"]
git-tree-sha1 = "8977b17906b0a1cc74ab2e3a05faa16cf08a8291"
uuid = "2913bbd2-ae8a-5f71-8c99-4fb6c76f3a91"
version = "0.33.16"

[[TOML]]
deps = ["Dates"]
git-tree-sha1 = "44aaac2d2aec4a850302f9aa69127c74f0c3787e"
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"
version = "1.0.3"

[[Test]]
deps = ["Distributed", "InteractiveUtils", "Logging", "Random"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[TimerOutputs]]
deps = ["ExprTools", "Printf"]
git-tree-sha1 = "d60b0c96a16aaa42138d5d38ad386df672cb8bd8"
uuid = "a759f4b9-e2f1-59dc-863e-4aeb61b1ea8f"
version = "0.5.16"

[[TranscodingStreams]]
deps = ["Random", "Test"]
git-tree-sha1 = "216b95ea110b5972db65aa90f88d8d89dcb8851c"
uuid = "3bb67fe8-82b1-5028-8e26-92a6c54297fa"
version = "0.9.6"

[[UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[ZipFile]]
deps = ["Libdl", "Printf", "Zlib_jll"]
git-tree-sha1 = "3593e69e469d2111389a9bd06bac1f3d730ac6de"
uuid = "a5390f91-8eb1-5f08-bee0-b1d1ffed6cea"
version = "0.9.4"

[[Zlib_jll]]
deps = ["Artifacts", "JLLWrappers", "Libdl", "Pkg"]
git-tree-sha1 = "320228915c8debb12cb434c59057290f0834dbf6"
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"
version = "1.2.11+18"

[[Zygote]]
deps = ["AbstractFFTs", "ChainRules", "ChainRulesCore", "DiffRules", "Distributed", "FillArrays", "ForwardDiff", "IRTools", "InteractiveUtils", "LinearAlgebra", "MacroTools", "NaNMath", "Random", "Requires", "SpecialFunctions", "Statistics", "ZygoteRules"]
git-tree-sha1 = "b1d95edd4e693066c38c13a10aab0a8f6a6e2f65"
uuid = "e88e6eb3-aa80-5325-afca-941959d7151f"
version = "0.6.12"

[[ZygoteRules]]
deps = ["MacroTools"]
git-tree-sha1 = "8c1a8e4dfacb1fd631745552c8db35d0deb09ea0"
uuid = "700de1a5-db45-46bc-99cf-38207098b444"
version = "0.2.2"
"""

# ╔═╡ Cell order:
# ╠═6e00b510-b456-11ec-2644-cb19a985b621
# ╠═d4223ac6-c16e-45e9-8432-8e0956ccc1e8
# ╠═093058f8-de9f-429b-ba45-d1d0714a8fcd
# ╠═719dc1b2-39f9-4b08-bf7c-d41737cd2b50
# ╠═f1cf90f2-8d88-4feb-8cf3-b8489e815655
# ╠═4f648f72-a656-492c-8ce0-c3a385b5bbf8
# ╠═17a44366-9ce4-4133-8b0f-031c30cd23c0
# ╠═ad79116d-6236-4654-8622-964642a0601a
# ╠═5b0bf080-4f73-42d3-900d-6a4cdc0d44e9
# ╠═4c532728-409b-4703-a4ad-527e7e2c34d6
# ╠═51a935b4-e8c2-4b7a-9a4c-4d1a3727ef96
# ╠═fd9c0e2e-b0e2-43a3-8881-da376b3e34b1
# ╠═3d1f84de-e5c9-4af4-b9cd-755ade20d9e2
# ╠═79fd5749-f46c-43db-9867-31a0ee1334ed
# ╠═94855084-0162-4c33-b542-b40a64f85bc9
# ╠═016e4ad3-b5f1-42a3-b084-b3957c9d4151
# ╠═b974f8a2-6965-4f5e-a396-b6175290af08
# ╠═abdfc4f3-c840-4934-a724-e9a0b09c0cc0
# ╠═18011983-1da8-47cf-b045-9bb02fad0056
# ╠═72d8caa6-07ef-4645-abc2-680f5762b90d
# ╠═5be1a79c-fae5-46ae-822f-83c88aa3ba04
# ╠═3e3e5a06-88fe-46b9-94f9-5f06244d30eb
# ╠═400d7709-d3d9-4161-a1f1-061a75c6079f
# ╠═6a06fdac-134a-45d7-8b7f-ab5e5ece5f7e
# ╠═528908c6-6853-4109-bb7f-447657f51da9
# ╠═e7fe8008-a01e-4483-aea8-a02f4c14f16e
# ╠═c2c466ea-f7df-44d5-816e-d6a68f9f3389
# ╠═3898d756-79c9-4026-bcda-aaf794f8bd36
# ╠═47ce70d2-f2ea-47ac-9762-b7684af064ea
# ╠═7af8e4ee-6754-4cdc-abdd-3b6dc2dec06d
# ╠═e1d7f5c2-cdd3-441f-942f-429c6355aa92
# ╠═dbf71386-4b85-438f-8c27-79879f1f1a47
# ╠═be6b4c29-97e0-411c-9d3c-a3dfb4de67a5
# ╠═c0b00506-d0e7-4f93-b338-26aa24fbc0e4
# ╠═3f39c934-bfc1-494e-940c-a5c1d3717a57
# ╠═4b50e0cb-23d4-40d1-a654-e0d856399138
# ╠═d6879c8c-5acc-4e74-b718-18bd975eef7e
# ╠═98608d66-8ec3-49c2-b7b8-03dfb8637a97
# ╠═22b96045-920b-4f56-9c3e-08e24034c658
# ╠═ab678882-fa81-4e45-b5dd-c56dce5da014
# ╠═67fa02a7-9a52-4289-8f11-c5f567da4385
# ╠═865306cd-7022-41e5-adcf-97736698e3b0
# ╠═13647710-d95e-4857-b8d8-15593a750c0a
# ╠═c3dcf1d7-83e2-48d2-8535-929be78ae051
# ╠═5e580bb1-91f0-4862-b93c-31d4c9da7598
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
